/**** Create by xiongbo.leo on 2023/3/22*****/
"use strict";

const vsSource = `
    attribute vec4 aVertexPosition;
    attribute vec2 aTextureCoord;

    uniform mat4 uModelViewMatrix;
    uniform mat4 uProjectionMatrix;

    varying highp vec2 vTextureCoord;

    void main(void) {
      gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
      vTextureCoord = aTextureCoord;
    }
  `;

// Fragment shader program

const fsSource = `
    varying highp vec2 vTextureCoord;

    uniform sampler2D uSampler;

    void main(void) {
      gl_FragColor = texture2D(uSampler, vTextureCoord);
    }
  `;

let cubePosBuffer,cubeCoordBuffer,cubeIdices ,cubeProgramInfo,cubetexture;

function initCube(gl) {
    initCubeShader(gl);
    initCubeBuffer(gl)

    cubetexture = ShaderProgram.loadTexture(gl,"cubetexture.png");
}

function initCubeShader(gl) {
    const shaderProgram = ShaderProgram.initShaderProgram(gl, vsSource, fsSource);
    cubeProgramInfo = {
        program: shaderProgram,
        attribLocations: {
            vertexPosition: gl.getAttribLocation(shaderProgram, 'aVertexPosition'),
            textureCoord: gl.getAttribLocation(shaderProgram, 'aTextureCoord')
        },
        uniformLocations: {
            projectionMatrix: gl.getUniformLocation(shaderProgram, 'uProjectionMatrix'),
            modelViewMatrix: gl.getUniformLocation(shaderProgram, 'uModelViewMatrix'),
            uSampler: gl.getUniformLocation(shaderProgram, 'uSampler')
        },
    }
}
function initCubeBuffer(gl) {
    cubePosBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,cubePosBuffer);
    // 六个面坐标
    const positions = [
        // Front face
        -1.0, -1.0,  1.0,
        1.0, -1.0,  1.0,
        1.0,  1.0,  1.0,
        -1.0,  1.0,  1.0,

        // Back face
        -1.0, -1.0, -1.0,
        -1.0,  1.0, -1.0,
        1.0,  1.0, -1.0,
        1.0, -1.0, -1.0,

        // Top face
        -1.0,  1.0, -1.0,
        -1.0,  1.0,  1.0,
        1.0,  1.0,  1.0,
        1.0,  1.0, -1.0,

        // Bottom face
        -1.0, -1.0, -1.0,
        1.0, -1.0, -1.0,
        1.0, -1.0,  1.0,
        -1.0, -1.0,  1.0,

        // Right face
        1.0, -1.0, -1.0,
        1.0,  1.0, -1.0,
        1.0,  1.0,  1.0,
        1.0, -1.0,  1.0,

        // Left face
        -1.0, -1.0, -1.0,
        -1.0, -1.0,  1.0,
        -1.0,  1.0,  1.0,
        -1.0,  1.0, -1.0,
    ];

    gl.bufferData(gl.ARRAY_BUFFER,new Float32Array(positions),gl.STATIC_DRAW);

    cubeCoordBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,cubeCoordBuffer);
    const textureCoordinates = [
        // Front
        0.0,  0.0,
        1.0,  0.0,
        1.0,  1.0,
        0.0,  1.0,
        // Back
        0.0,  0.0,
        1.0,  0.0,
        1.0,  1.0,
        0.0,  1.0,
        // Top
        0.0,  0.0,
        1.0,  0.0,
        1.0,  1.0,
        0.0,  1.0,
        // Bottom
        0.0,  0.0,
        1.0,  0.0,
        1.0,  1.0,
        0.0,  1.0,
        // Right
        0.0,  0.0,
        1.0,  0.0,
        1.0,  1.0,
        0.0,  1.0,
        // Left
        0.0,  0.0,
        1.0,  0.0,
        1.0,  1.0,
        0.0,  1.0,
    ];
    gl.bufferData(gl.ARRAY_BUFFER,new Float32Array(textureCoordinates),gl.STATIC_DRAW);

    cubeIdices = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER,cubeIdices);
    const indices = [
        0,  1,  2,      0,  2,  3,    // front
        4,  5,  6,      4,  6,  7,    // back
        8,  9,  10,     8,  10, 11,   // top
        12, 13, 14,     12, 14, 15,   // bottom
        16, 17, 18,     16, 18, 19,   // right
        20, 21, 22,     20, 22, 23,   // left
    ];
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER,new Uint16Array(indices),gl.STATIC_DRAW);

}

function drawCube(gl,dt) {
    const fieldOfView = 45 * Math.PI /180;
    const aspect = gl.canvas.clientWidth/gl.canvas.clientHeight;
    const zNear = 0.1;
    const zFar = 100.0;
    const projectionMatrix = mat4.create();

    mat4.perspective(projectionMatrix,fieldOfView,aspect,zNear,zFar);
    const modelViewMatrix = mat4.create();

    // 平移
    mat4.translate(modelViewMatrix,modelViewMatrix,[-0.0,0.0,-10.0])
    // 旋转  cubeRotation 沿 z轴
    mat4.rotate(modelViewMatrix,modelViewMatrix,cubeRotation,[0,0,1]);

    mat4.rotate(modelViewMatrix,modelViewMatrix,cubeRotation*.7,[0,1,0]);
    {
        gl.bindBuffer(gl.ARRAY_BUFFER,cubePosBuffer);
        gl.vertexAttribPointer(cubeProgramInfo.attribLocations.vertexPosition,3,gl.FLOAT,false,0,0);
        gl.enableVertexAttribArray(cubeProgramInfo.attribLocations.vertexPosition);
    }

    {
        gl.bindBuffer(gl.ARRAY_BUFFER,cubeCoordBuffer);
        gl.vertexAttribPointer(cubeProgramInfo.attribLocations.textureCoord,
            2,
            gl.FLOAT,
            false,
            0,0);
        gl.enableVertexAttribArray(cubeProgramInfo.attribLocations.textureCoord);
    }



    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER,cubeIdices);
    gl.useProgram(cubeProgramInfo.program);
    gl.uniformMatrix4fv(cubeProgramInfo.uniformLocations.projectionMatrix,false,projectionMatrix);
    gl.uniformMatrix4fv(cubeProgramInfo.uniformLocations.modelViewMatrix,false,modelViewMatrix);

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D,cubetexture);
    gl.uniform1i(cubeProgramInfo.uniformLocations.uSampler,0);
    {
        const vertexCount = 36;
        gl.drawElements(gl.TRIANGLES,vertexCount,gl.UNSIGNED_SHORT,0);
    }

    cubeRotation += dt;

}
