/**** Create by xiongbo.leo on 2023/3/23*****/
"use strict";


let panelBuffer, panelIndice , PanelProgramInfo;
const panelVsSource = `
    attribute vec4 aVertexPosition;
    
    attribute vec2 aTextureCoord;
    varying highp vec2 vTextureCoord;
    void main(void) {
      gl_Position =  aVertexPosition;
      vTextureCoord = aTextureCoord;
    }
  `;


const panelFsSource = `
    varying highp vec2 vTextureCoord;

    uniform sampler2D uSampler;

    void main(void) {
      gl_FragColor = texture2D(uSampler, vTextureCoord);
    }
  `;


function initPanel(gl) {
    initPanelBuffers(gl)
    initPanelShader(gl)

}

function initPanelShader(gl){
    const panelProgram = ShaderProgram.initShaderProgram(gl,panelVsSource,panelFsSource);
    PanelProgramInfo = {
        panelProgram: panelProgram,
        panelAttribLocations:{
            vertexPosition:gl.getAttribLocation(panelProgram,'aVertexPosition'),
            textureCoord:gl.getAttribLocation(panelProgram,'aTextureCoord')
        },
        panelUniformLocations:{
            uSampler:gl.getUniformLocation(panelProgram,'uSampler')
        },
    };
}

function drawPanel(gl,texture) {
    let  programInfo= PanelProgramInfo;
    gl.bindBuffer(gl.ARRAY_BUFFER,panelBuffer);
    gl.vertexAttribPointer(programInfo.panelAttribLocations.vertexPosition,2,gl.FLOAT,false,16,0);
    gl.enableVertexAttribArray(programInfo.panelAttribLocations.vertexPosition);

    gl.vertexAttribPointer(programInfo.panelAttribLocations.textureCoord,2,gl.FLOAT,false,16,8);
    gl.enableVertexAttribArray(programInfo.panelAttribLocations.textureCoord);
    gl.useProgram(programInfo.panelProgram);
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER,panelIndice);


    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D,texture);
    gl.uniform1i(programInfo.panelUniformLocations.uSampler,0);
    gl.drawElements(gl.TRIANGLES,6,gl.UNSIGNED_SHORT,0)

}


function initPanelBuffers(gl) {
    // 平面
    panelBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,panelBuffer);
    const panelPosition = [
        0.5,0.5,    1.0,1.0,
        0.5,-0.5,   1.0,0.0,
        -0.5,-0.5,  0.0,0.0,
        -0.5,0.5,   0.0,1.0
    ];

    gl.bufferData(gl.ARRAY_BUFFER,new Float32Array(panelPosition),gl.STATIC_DRAW);

    panelIndice = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER,panelIndice);
    const panelIndicePos = [
       0,1,2,
       2,0,3
    ]
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER,new Uint16Array(panelIndicePos),gl.STATIC_DRAW);

}

