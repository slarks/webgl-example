var cubeRotation = 0.0;
main()
function main() {
    const gl = ShaderProgram.getWebGl();

    const vsSource = `
    attribute vec4 aVertexPosition;
    attribute vec2 aTextureCoord;

    uniform mat4 uModelViewMatrix;
    uniform mat4 uProjectionMatrix;

    varying highp vec2 vTextureCoord;

    void main(void) {
      gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
      vTextureCoord = aTextureCoord;
    }
  `;

    // Fragment shader program

    const fsSource = `
    varying highp vec2 vTextureCoord;

    uniform sampler2D uSampler;

    void main(void) {
      gl_FragColor = texture2D(uSampler, vTextureCoord);
    }
  `;
    const shaderProgram = ShaderProgram.initShaderProgram(gl,vsSource,fsSource);
    const programInfo = {
        program:shaderProgram,
        attribLocations:{
            vertexPosition:gl.getAttribLocation(shaderProgram,'aVertexPosition'),
            textureCoord:gl.getAttribLocation(shaderProgram,'aTextureCoord')
        },
        uniformLocations:{
            projectionMatrix:gl.getUniformLocation(shaderProgram,'uProjectionMatrix'),
            modelViewMatrix:gl.getUniformLocation(shaderProgram,'uModelViewMatrix'),
            uSampler:gl.getUniformLocation(shaderProgram,'uSampler')
        }
    };

    const buffers = initBuffers(gl);
    const  texture = ShaderProgram.loadTexture(gl,"cubetexture.png");
    var then = 0;
    function render(now) {
        now *= 0.001;
        const deltaTime=now - then;
        then = now;
        drawScene(gl,programInfo,buffers,texture,deltaTime);
        requestAnimationFrame(render);
    }
    requestAnimationFrame(render);
}

function initBuffers(gl) {
    const positionBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,positionBuffer);
    // 六个面坐标
    const positions = [
        // Front face
        -1.0, -1.0,  1.0,
        1.0, -1.0,  1.0,
        1.0,  1.0,  1.0,
        -1.0,  1.0,  1.0,

        // Back face
        -1.0, -1.0, -1.0,
        -1.0,  1.0, -1.0,
        1.0,  1.0, -1.0,
        1.0, -1.0, -1.0,

        // Top face
        -1.0,  1.0, -1.0,
        -1.0,  1.0,  1.0,
        1.0,  1.0,  1.0,
        1.0,  1.0, -1.0,

        // Bottom face
        -1.0, -1.0, -1.0,
        1.0, -1.0, -1.0,
        1.0, -1.0,  1.0,
        -1.0, -1.0,  1.0,

        // Right face
        1.0, -1.0, -1.0,
        1.0,  1.0, -1.0,
        1.0,  1.0,  1.0,
        1.0, -1.0,  1.0,

        // Left face
        -1.0, -1.0, -1.0,
        -1.0, -1.0,  1.0,
        -1.0,  1.0,  1.0,
        -1.0,  1.0, -1.0,
    ];

    gl.bufferData(gl.ARRAY_BUFFER,new Float32Array(positions),gl.STATIC_DRAW);

    const textureCoordBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,textureCoordBuffer);
    const textureCoordinates = [
        // Front
        0.0,  0.0,
        1.0,  0.0,
        1.0,  1.0,
        0.0,  1.0,
        // Back
        0.0,  0.0,
        1.0,  0.0,
        1.0,  1.0,
        0.0,  1.0,
        // Top
        0.0,  0.0,
        1.0,  0.0,
        1.0,  1.0,
        0.0,  1.0,
        // Bottom
        0.0,  0.0,
        1.0,  0.0,
        1.0,  1.0,
        0.0,  1.0,
        // Right
        0.0,  0.0,
        1.0,  0.0,
        1.0,  1.0,
        0.0,  1.0,
        // Left
        0.0,  0.0,
        1.0,  0.0,
        1.0,  1.0,
        0.0,  1.0,
    ];
    gl.bufferData(gl.ARRAY_BUFFER,new Float32Array(textureCoordinates),gl.STATIC_DRAW);

    const indexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER,indexBuffer);
    const indices = [
        0,  1,  2,      0,  2,  3,    // front
        4,  5,  6,      4,  6,  7,    // back
        8,  9,  10,     8,  10, 11,   // top
        12, 13, 14,     12, 14, 15,   // bottom
        16, 17, 18,     16, 18, 19,   // right
        20, 21, 22,     20, 22, 23,   // left
    ];
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER,new Uint16Array(indices),gl.STATIC_DRAW);
    return {
        position:positionBuffer,
        textureCoord:textureCoordBuffer,
        indices:indexBuffer,
    }

}

function drawScene(gl,programInfo,buffers,texture,dt) {
    gl.clearColor(0.0, 0.0, 0.0, 1.0);  // Clear to black, fully opaque
    gl.clearDepth(1.0);                 // Clear everything
    gl.enable(gl.DEPTH_TEST);           // Enable depth testing
    gl.depthFunc(gl.LEQUAL);            // Near things obscure far things

    // Clear the canvas before we start drawing on it.

    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    const fieldOfView = 45 * Math.PI /180;
    const aspect = gl.canvas.clientWidth/gl.canvas.clientHeight;
    const zNear = 0.1;
    const zFar = 100.0;
    const projectionMatrix = mat4.create();

    mat4.perspective(projectionMatrix,fieldOfView,aspect,zNear,zFar);
    const modelViewMatrix = mat4.create();

    // 平移
    mat4.translate(modelViewMatrix,modelViewMatrix,[-0.0,0.0,-10.0])
    // 旋转  cubeRotation 沿 z轴
    mat4.rotate(modelViewMatrix,modelViewMatrix,cubeRotation,[0,0,1]);

    mat4.rotate(modelViewMatrix,modelViewMatrix,cubeRotation*.7,[0,1,0]);
    {
        gl.bindBuffer(gl.ARRAY_BUFFER,buffers.position);
        gl.vertexAttribPointer(programInfo.attribLocations.vertexPosition,3,gl.FLOAT,false,0,0);
        gl.enableVertexAttribArray(programInfo.attribLocations.vertexPosition);
    }

    {
        gl.bindBuffer(gl.ARRAY_BUFFER,buffers.textureCoord);
        gl.vertexAttribPointer(programInfo.attribLocations.textureCoord,
            2,
            gl.FLOAT,
            false,
            0,0);
        gl.enableVertexAttribArray(programInfo.attribLocations.textureCoord);
    }



    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER,buffers.indices);
    gl.useProgram(programInfo.program);
    gl.uniformMatrix4fv(programInfo.uniformLocations.projectionMatrix,false,projectionMatrix);
    gl.uniformMatrix4fv(programInfo.uniformLocations.modelViewMatrix,false,modelViewMatrix);

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D,texture);
    gl.uniform1i(programInfo.uniformLocations.uSampler,0);
    {
        const vertexCount = 36;
        gl.drawElements(gl.TRIANGLES,vertexCount,gl.UNSIGNED_SHORT,0);
    }

    cubeRotation += dt;

}
