/**** Create by xiongbo.leo on 2023/3/22*****/
"use strict";

/***
 *    webgl 2.0  实现了抗锯齿效果
 * @type {number}
 */
var cubeRotation = 0.0;

let  frameBufferWidth = 256, frameBufferHeight = 256;
let  frameTexture;
let renderableFrameBuffer, colorFrameBuffer;

let canvasWidth = 100, canvasHeight = 100;
main()
function main() {
    const gl = ShaderProgram.getWebGl();
    const canvas = document.querySelector("#glcanvas");
    canvasWidth = canvas.width;
    canvasHeight = canvas.height;

    initCube(gl);
    initPanel(gl);

    initFrameBuffer(gl)
    var then = 0;
    function render(now) {
        now *= 0.001;
        const deltaTime=now - then;
        then = now;
        drawScene(gl,deltaTime);
        requestAnimationFrame(render);
    }
    requestAnimationFrame(render);
}


function initFrameBuffer(gl) {

    // 创建 colorFrameBuffer；
    let targetTexture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D,targetTexture);
    gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,frameBufferWidth,frameBufferHeight,0,gl.RGBA,gl.UNSIGNED_BYTE,null);
    gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MIN_FILTER,gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MAG_FILTER,gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_S,gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_T,gl.CLAMP_TO_EDGE);
    frameTexture = targetTexture;


    colorFrameBuffer = gl.createFramebuffer();
    gl.bindFramebuffer(gl.FRAMEBUFFER,colorFrameBuffer);
    let attachmentPoint = gl.COLOR_ATTACHMENT0;
    gl.framebufferTexture2D(gl.FRAMEBUFFER,attachmentPoint,gl.TEXTURE_2D,targetTexture,0);
    let depthBuffer1 = gl.createRenderbuffer();
    gl.bindRenderbuffer(gl.RENDERBUFFER,depthBuffer1);
    gl.renderbufferStorage(gl.RENDERBUFFER,gl.DEPTH_COMPONENT16,frameBufferWidth,frameBufferHeight);
    gl.framebufferRenderbuffer(gl.FRAMEBUFFER,gl.DEPTH_ATTACHMENT,gl.RENDERBUFFER,depthBuffer1);
    gl.bindFramebuffer(gl.FRAMEBUFFER,null);


    // 创建 renderableFrameBuffer；
    let samplerCount = 4;
    renderableFrameBuffer = gl.createFramebuffer();
    gl.bindFramebuffer(gl.FRAMEBUFFER,renderableFrameBuffer);
    let textureRenderBuffer = gl.createRenderbuffer();
    gl.bindRenderbuffer(gl.RENDERBUFFER,textureRenderBuffer);
    // 多重采样
    gl.renderbufferStorageMultisample(gl.RENDERBUFFER,samplerCount,gl.RGBA8,frameBufferWidth,frameBufferHeight)
    gl.framebufferRenderbuffer(gl.FRAMEBUFFER,gl.COLOR_ATTACHMENT0,gl.RENDERBUFFER,textureRenderBuffer);

    let depthBuffer = gl.createRenderbuffer();
    gl.bindRenderbuffer(gl.RENDERBUFFER,depthBuffer);
    gl.renderbufferStorageMultisample(gl.RENDERBUFFER,samplerCount,gl.DEPTH_COMPONENT16,frameBufferWidth,frameBufferHeight);
    gl.framebufferRenderbuffer(gl.FRAMEBUFFER,gl.DEPTH_ATTACHMENT,gl.RENDERBUFFER,depthBuffer);
    gl.bindFramebuffer(gl.FRAMEBUFFER,null);
}


function drawScene(gl,dt) {

    gl.clearDepth(1.0);                 // Clear everything
    gl.enable(gl.DEPTH_TEST);           // Enable depth testing
    gl.depthFunc(gl.LEQUAL);            // Near things obscure far things

    gl.bindFramebuffer(gl.FRAMEBUFFER,renderableFrameBuffer);
    gl.viewport(0,0,frameBufferWidth,frameBufferHeight);
    gl.clearColor(0.0, 1.0, 0.0, 1.0);  // Clear to black, fully opaque
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    drawCube(gl,dt)
    gl.bindFramebuffer(gl.FRAMEBUFFER,null);


    gl.bindFramebuffer(gl.READ_FRAMEBUFFER,renderableFrameBuffer);
    gl.bindFramebuffer(gl.DRAW_FRAMEBUFFER,colorFrameBuffer)
    //gl.clearBufferfv(gl.COLOR,0,[0.0,0.0,0.0,1.0]);
    gl.blitFramebuffer(
        0,0,frameBufferWidth,frameBufferHeight,
        0,0,frameBufferWidth,frameBufferHeight,
        gl.COLOR_BUFFER_BIT,gl.NEAREST
    )
    gl.bindFramebuffer(gl.READ_FRAMEBUFFER,null);
    gl.bindFramebuffer(gl.DRAW_FRAMEBUFFER,null)


    gl.viewport(0,0,canvasWidth,canvasHeight);
    gl.clearColor(1.0, 0.0, 0.0, 1.0);  // Clear to black, fully opaque
    //gl.clearDepth(1.0);                 // Clear everything
    drawPanel(gl,frameTexture);
}
