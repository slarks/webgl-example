/**** Create by xiongbo.leo on 2023/3/22*****/
"use strict";

/***
 *    webgl 1.0   离屏渲染无法使用抗锯齿
 * @type {number}
 */
var cubeRotation = 0.0;

let  frameBufferWidth = 256, frameBufferHeight = 256;
let framebuffer, frameTexture;

let canvasWidth = 100, canvasHeight = 100;
main()
function main() {
    const gl = ShaderProgram.getWebGl();
    const canvas = document.querySelector("#glcanvas");
    canvasWidth = canvas.width;
    canvasHeight = canvas.height;

    initCube(gl);
    initPanel(gl);

    initFrameBuffer(gl)
    var then = 0;
    function render(now) {
        now *= 0.001;
        const deltaTime=now - then;
        then = now;
        drawScene(gl,deltaTime);
        requestAnimationFrame(render);
    }
    requestAnimationFrame(render);
}


function initFrameBuffer(gl) {
    let targetTexture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D,targetTexture);

    gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,frameBufferWidth,frameBufferHeight,0,gl.RGBA,gl.UNSIGNED_BYTE,null);
    gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MIN_FILTER,gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MAG_FILTER,gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_S,gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_T,gl.CLAMP_TO_EDGE);

    let fb = gl.createFramebuffer();
    gl.bindFramebuffer(gl.FRAMEBUFFER,fb);
    let attachmentPoint = gl.COLOR_ATTACHMENT0;
    gl.framebufferTexture2D(gl.FRAMEBUFFER,attachmentPoint,gl.TEXTURE_2D,targetTexture,0);
    framebuffer = fb;
    frameTexture = targetTexture;


    let depthBuffer = gl.createRenderbuffer();
    gl.bindRenderbuffer(gl.RENDERBUFFER,depthBuffer);
    gl.renderbufferStorage(gl.RENDERBUFFER,gl.DEPTH_COMPONENT16,frameBufferWidth,frameBufferHeight);
    gl.framebufferRenderbuffer(gl.FRAMEBUFFER,gl.DEPTH_ATTACHMENT,gl.RENDERBUFFER,depthBuffer);
    gl.bindFramebuffer(gl.FRAMEBUFFER,null);
}


function drawScene(gl,dt) {

    gl.clearDepth(1.0);                 // Clear everything
    gl.enable(gl.DEPTH_TEST);           // Enable depth testing
    gl.depthFunc(gl.LEQUAL);            // Near things obscure far things

    gl.bindFramebuffer(gl.FRAMEBUFFER,framebuffer);
    gl.viewport(0,0,frameBufferWidth,frameBufferHeight);

    gl.clearColor(1.0, 0.0, 0.0, 1.0);  // Clear to black, fully opaque
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    drawCube(gl,dt)
    gl.bindFramebuffer(gl.FRAMEBUFFER,null);

    gl.disable(gl.DEPTH_TEST);           // Enable depth testing
    gl.viewport(0,0,canvasWidth,canvasHeight);
    gl.clearColor(1.0, 0.0, 0.0, 1.0);  // Clear to black, fully opaque
    gl.clearDepth(1.0);                 // Clear everything
    drawPanel(gl,frameTexture);
}
