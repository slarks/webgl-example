
function main() {
    const canvas = document.querySelector('#glcanvas');
    const gl = canvas.getContext('webgl');

    if(!gl){
        return;
    }

    // Vertex shader program

    const vsSource = `
    attribute vec4 aVertexPosition;

    uniform mat4 uModelViewMatrix;
    uniform mat4 uProjectionMatrix;

    void main() {
      gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
    }
  `;
    // Fragment shader program

    const fsSource = `
    void main() {
      gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
    }
  `;


    const shaderProgram = initShaderProgram(gl,vsSource,fsSource);
    const programInfo = {
        program:shaderProgram,
        attribLocations:{
            vertexPosition:gl.getAttribLocation(shaderProgram,'aVertexPosition')
        },
        uniformLocations:{
            projectionMatrix:gl.getUniformLocation(shaderProgram,'uProjectionMatrix'),
            modelViewMatrix: gl.getUniformLocation(shaderProgram, 'uModelViewMatrix'),
        }
    };

    const buffers = initBuffers(gl);
    drawScene(gl,programInfo,buffers);

}


function drawScene(gl,programInfo,buffers) {
    gl.clearColor(0.0,0.0,0.0,1.0);
    gl.clearDepth(1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);

    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    const fieldOfView = 45;
    const aspect = gl.canvas.clientWidth/gl.canvas.clientHeight;
    const zNear = .1;
    const zFar = 100.0;
    const projectMatrix = mat4.create();
    mat4.perspective(projectMatrix,
        fieldOfView,
        aspect,
        zNear,
        zFar);

    const modelViewMatrix = mat4.create();
    mat4.translate(modelViewMatrix,modelViewMatrix,[-0.0,0.0,-0.6]);
    {
        gl.bindBuffer(gl.ARRAY_BUFFER,buffers.position);
        gl.vertexAttribPointer(programInfo.attribLocations.vertexPosition,2,gl.FLOAT,false,0,0);
        gl.enableVertexAttribArray(programInfo.attribLocations.vertexPosition);

    }
    gl.useProgram(programInfo.program);

    gl.uniformMatrix4fv(programInfo.uniformLocations.projectionMatrix,false,projectMatrix);
    gl.uniformMatrix4fv(programInfo.uniformLocations.modelViewMatrix,false,modelViewMatrix);
    gl.drawArrays(gl.TRIANGLE_STRIP,0,4);


}

function initBuffers(gl) {
    const positionBuffer = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER,positionBuffer);

    const positions = [
        1.0 ,1.0,
        -1.0,1.0,
        1.0, -1.0,
        -1.0,-1.0,
    ];

    gl.bufferData(gl.ARRAY_BUFFER,new Float32Array(positions),gl.STATIC_DRAW);

    return{
        position:positionBuffer,
    }
}


function initShaderProgram(gl,vSource,fSource) {
    const vShader = loadShader(gl,gl.VERTEX_SHADER,vSource);
    const fShader = loadShader(gl,gl.FRAGMENT_SHADER,fSource);

    const shaderProgram = gl.createProgram();
    gl.attachShader(shaderProgram,vShader);
    gl.attachShader(shaderProgram,fShader);
    gl.linkProgram(shaderProgram);

    if(!gl.getProgramParameter(shaderProgram,gl.LINK_STATUS)){
        return null;
    }
    return shaderProgram;
}

function loadShader(gl,type,source) {
    const shader = gl.createShader(type);
    gl.shaderSource(shader,source);
    gl.compileShader(shader);

    if(!gl.getShaderParameter(shader,gl.COMPILE_STATUS)){
        gl.deleteShader(shader);
        return;
    }
    return shader;
}

main();
