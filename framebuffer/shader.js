
var ShaderProgram = {
    loadShader:function (gl,type,source) {
        const shader = gl.createShader(type);
        gl.shaderSource(shader,source);
        gl.compileShader(shader);
        if(!gl.getShaderParameter(shader,gl.COMPILE_STATUS)){
            alert("shader error")
            return;
        }
        return shader;
    },
    initShaderProgram:function(gl,vSource,fSouce) {
        const vShader = this.loadShader(gl,gl.VERTEX_SHADER,vSource);
        const fShader = this.loadShader(gl,gl.FRAGMENT_SHADER,fSouce);
        const program = gl.createProgram();
        gl.attachShader(program,vShader);
        gl.attachShader(program,fShader);
        gl.linkProgram(program);
        if(!gl.getProgramParameter(program,gl.LINK_STATUS)){
            alert("link error")
            return;
        }
        return program;
    },


    getContext:function  (canvas) {
        let context = null;
        try {
            const webGLCtxAttribs = {
                alpha: false,
                antialias: true,
                depth: true,
                stencil: true,
                premultipliedAlpha: false,
                preserveDrawingBuffer: false,
                powerPreference: 'default',
                failIfMajorPerformanceCaveat: false,
            };
            context = canvas.getContext('webgl2', webGLCtxAttribs);
        } catch (err) {
            return null;
        }
        return context;
    },

    getWebGl:function() {
        const canvas = document.querySelector("#glcanvas");
        return this.getContext(canvas)
    },
    loadTexture:function (gl,url) {
        const texture = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_2D,texture);
        const level = 0;
        const internalFormat = gl.RGBA;
        const width=1,height =1,border = 0,srcFormat = gl.RGBA;
        const srcType = gl.UNSIGNED_BYTE;
        const pixel = new Uint8Array([0,0,255,255]);
        gl.texImage2D(gl.TEXTURE_2D,level,internalFormat,width,height,border,srcFormat,srcType,pixel)
        const image = new Image();
        image.onload = function () {
            gl.bindTexture(gl.TEXTURE_2D,texture);
            gl.texImage2D(gl.TEXTURE_2D,level,internalFormat,srcFormat,srcType,image);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        }
        image.src =url;
        return texture;
    },
}





